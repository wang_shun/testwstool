//
//  WSToolManager.h
//  FBSnapshotTestCase
//
//  Created by wangshunWeiXing on 2020/9/10.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface WSToolManager : NSObject

- (void)testManager;

@end

NS_ASSUME_NONNULL_END
