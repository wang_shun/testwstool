# WSTool

[![CI Status](https://img.shields.io/travis/wangshun/WSTool.svg?style=flat)](https://travis-ci.org/wangshun/WSTool)
[![Version](https://img.shields.io/cocoapods/v/WSTool.svg?style=flat)](https://cocoapods.org/pods/WSTool)
[![License](https://img.shields.io/cocoapods/l/WSTool.svg?style=flat)](https://cocoapods.org/pods/WSTool)
[![Platform](https://img.shields.io/cocoapods/p/WSTool.svg?style=flat)](https://cocoapods.org/pods/WSTool)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

WSTool is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'WSTool'
```

## Author

wangshun, 1269188410@qq.com

## License

WSTool is available under the MIT license. See the LICENSE file for more info.
