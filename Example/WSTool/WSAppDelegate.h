//
//  WSAppDelegate.h
//  WSTool
//
//  Created by wangshun on 09/06/2020.
//  Copyright (c) 2020 wangshun. All rights reserved.
//

@import UIKit;

@interface WSAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
