//
//  main.m
//  WSTool
//
//  Created by wangshun on 09/06/2020.
//  Copyright (c) 2020 wangshun. All rights reserved.
//

@import UIKit;
#import "WSAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([WSAppDelegate class]));
    }
}
